$( document ).ready(function() {

    $("#searchSettings").mCustomScrollbar({theme:"rounded-dark"});

    $('.hlink').click(function(){window.open($(this).data('link'));return false;});
    // twemoji.parse(document.body);

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type)) {
                console.log(getCookie('csrftoken'));
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });
    $("#id_search").addClass("input-lg");

    function addPopover () {

      $('.hlink').click(function(){window.open($(this).data('link'));return false;});

      $( ".recipe-tumbnail" ).unbind();
      $( ".infoButton" ).unbind();

      var infoButtons = $('.infoButton');
      if (!infoButtons.length) {
        $( ".recipe-tumbnail" ).hover(function() {
            var $this = $(this);

            $this.find(".recipe-additional").toggle({"duration" : 200});
        });
      } else {
        $('[data-toggle=popover]').each(function() {
            var $this = $(this);

            $this.find('.infoButton').click(function () {
                $this.find(".recipe-additional").toggle({"duration" : 200});
                $this.find(".infoButton").toggleClass("show-info");
                $this.find(".infoButton").toggleClass("hide-info");
            });
        });
      }

    }

    if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

    addPopover();

    var page = 0;

    // $("#searchSettings").hide();

    // $('#id_r_types').multiselect({
    //     buttonText: function(options, select) {
    //             if (options.length === 0) {
    //                 return 'Тип блюда';
    //             }
    //             else if (options.length > 5) {
    //                 return 'Больше пяти';
    //             }
    //              else {
    //                  var labels = [];
    //                  options.each(function() {
    //                      if ($(this).attr('label') !== undefined) {
    //                          labels.push($(this).attr('label'));
    //                      }
    //                      else {
    //                          labels.push($(this).html());
    //                      }
    //                  });
    //                  return labels.join(', ') + '';
    //              }
    //     }
    // });

    $('#id_r_types').imageselect({label: "Тип блюда"});
    //$('#id_ingredient_type').imageselect({label: "Тип ингредиентов"});
    $('#id_timelencategory').imageselect({label: "Время/количество ингредиентов"});

//    $('#id_diet').multiselect({
//        buttonText: function(options, select) {
//                if (options.length === 0) {
//                    return 'Диета';
//                }
//                else if (options.length > 5) {
//                    return 'Больше пяти';
//                }
//                 else {
//                     var labels = [];
//                     options.each(function() {
//                         if ($(this).attr('label') !== undefined) {
//                             labels.push($(this).attr('label'));
//                         }
//                         else {
//                             labels.push($(this).html());
//                         }
//                     });
//                     return labels.join(', ') + '';
//                 }
//        }
//    });

//    $('#id_ingredient_type').multiselect({
//        buttonText: function(options, select) {
//                if (options.length === 0) {
//                    return 'Тип ингредиентов';
//                }
//                else if (options.length > 5) {
//                    return 'Больше пяти';
//                }
//                 else {
//                     var labels = [];
//                     options.each(function() {
//                         if ($(this).attr('label') !== undefined) {
//                             labels.push($(this).attr('label'));
//                         }
//                         else {
//                             labels.push($(this).html());
//                         }
//                     });
//                     return labels.join(', ') + '';
//                 }
//        }
//    });

    $('#id_cuisine').multiselect({
        buttonText: function(options, select) {
                if (options.length === 0) {
                    return 'Кухня';
                }
                else if (options.length > 5) {
                    return 'Больше пяти';
                }
                 else {
                     var labels = [];
                     options.each(function() {
                         if ($(this).attr('label') !== undefined) {
                             labels.push($(this).attr('label'));
                         }
                         else {
                             labels.push($(this).html());
                         }
                     });
                     return labels.join(', ') + '';
                 }
        },
        // enableFiltering: true,
        // filterBehavior: 'value',
        maxHeight: 300
    });

//    $('#id_timelencategory').multiselect({
//        buttonText: function(options, select) {
//                if (options.length === 0) {
//                    return 'Приготовление';
//                }
//                else if (options.length > 5) {
//                    return 'Больше пяти';
//                }
//                 else {
//                     var labels = [];
//                     options.each(function() {
//                         if ($(this).attr('label') !== undefined) {
//                             labels.push($(this).attr('label'));
//                         }
//                         else {
//                             labels.push($(this).html());
//                         }
//                     });
//                     return labels.join(', ') + '';
//                 }
//        },
//        // enableFiltering: true,
//        // filterBehavior: 'value',
//        maxHeight: 300
//    });

    $('#sendMail').submit( function() {
        $.ajax({
            url     : "/sendMail/",
            type    : "POST",
            data    : $(this).serialize(),
        }).done(function( data ) {
            if (data === "ok") {
                console.log(data);
                $("#sendMail")[0].reset();
                $('#modalMail').modal('hide');
                $('#thankYou').modal('show');
            }
        });

        return false;
    });

        $('#getRecipeElastic').submit( function() {
            page = 0;
            scrollBlocked = false;

            $.ajax({
                url     : "/guesselastic/",
                type    : "GET",
                data    : $(this).serialize(),
                beforeSend : function(jqXHR, settings ) {
                    $( ".spell_suggestion" ).remove();
                    $( ".recipe-tumbnail" ).remove();
                    $( "#recipes" ).append( "<h1> <img src = '/static/img/loading.gif'/> </h1>");
                    window.history.pushState("object or string", "Title", settings.url+"&page="+page);
                },
                success : function(response) {
                    $( "#recipes h1" ).remove();
                    var tempDom = $('<output>').append($.parseHTML(response));
                    var appContainer = $('.recipes-wrapper #recipes', tempDom);
                    $( ".recipes-wrapper").append( appContainer);
                    document.title = tempDom.find("title").text();
                    addPopover();
                    $(".btn-vk").attr("href", "https://vk.com/share.php?url="+window.location.href);
                    $(".btn-twi").attr("href", "https://twitter.com/home?status"+window.location.href);
                }
            });

            return false;
        });
        $('#getRecipe').submit( function() {
            page = 0;
            scrollBlocked = false;

            $.ajax({
                url     : "/guessrecipe/",
                type    : "GET",
                data    : $(this).serialize(),
                beforeSend : function(jqXHR, settings ) {
                    $( ".spell_suggestion" ).remove();
                    $( ".recipe-tumbnail" ).remove();
                    $( "#recipes" ).append( "<h1> <img src = '/static/img/loading.gif'/> </h1>");
                    window.history.pushState("object or string", "Title", settings.url+"&page="+page);
                },
                success : function(response) {
                    $( "#recipes h1" ).remove();
                    var tempDom = $('<output>').append($.parseHTML(response));
                    var appContainer = $('.recipes-wrapper #recipes', tempDom);
                    $( ".recipes-wrapper").append( appContainer);
                    document.title = tempDom.find("title").text();
                    addPopover();
                    $(".btn-vk").attr("href", "https://vk.com/share.php?url="+window.location.href);
                    $(".btn-twi").attr("href", "https://twitter.com/home?status"+window.location.href);
                }
            });

            return false;
        });

    var loading = false;

    var scrollBlocked = false;

    $(document).scroll(function () {
        var triggerScroll = $(window).scrollTop() + $(window).height() >= $(document).height();
        var count = $('.recipes-wrapper #recipes').find(".recipe-tumbnail").length || 0;
        if  ( ($(window).scrollTop() >= ( $(document).height() - $(window).height()) - 60)&& !loading && count > 0 && !scrollBlocked){

            $.ajax({
                url     : "/guessrecipe/",
                type    : "GET",
                data    : $(this).serialize(),
                beforeSend : function(jqXHR, settings ) {
                    loading = true;
                    $( "#recipes" ).append( "<h1> <img src = '/static/img/loading.gif'/> </h1>");
                    window.history.pushState("object or string", "Title", window.location.search.replace('page='+page,'page='+(++page)));
                    settings.url = window.location.search;
                },
                success : function(response) {
                    $( "#recipes h1" ).remove();
                    var tempDom = $('<output>').append($.parseHTML(response));
                    var appContainer = $('.recipes-wrapper #recipes', tempDom);
                    count = appContainer.find(".recipe-tumbnail").length;
                    if (count === 0) {
                        window.history.pushState("object or string", "Title", window.location.search.replace('page='+page,'page='+(--page)));
                        scrollBlocked = true;
                    }
                    else {
                      $( ".recipes-wrapper" ).append( appContainer);
                      addPopover();
                    }
                    loading = false;

                }
            });
        }
    });

    $(document).scroll(function () {
        var triggerScroll = $(window).scrollTop() + $(window).height() >= $(document).height();
        var count = $('.recipes-wrapper #recipes').find(".recipe-tumbnail").length || 0;
        if  ( ($(window).scrollTop() >= ( $(document).height() - $(window).height()) - 60)&& !loading && count > 0 && !scrollBlocked){

            $.ajax({
                url     : "/guesselastic/",
                type    : "GET",
                data    : $(this).serialize(),
                beforeSend : function(jqXHR, settings ) {
                    loading = true;
                    $( "#recipes" ).append( "<h1> <img src = '/static/img/loading.gif'/> </h1>");
                    window.history.pushState("object or string", "Title", window.location.search.replace('page='+page,'page='+(++page)));
                    settings.url = window.location.search;
                },
                success : function(response) {
                    $( "#recipes h1" ).remove();
                    var tempDom = $('<output>').append($.parseHTML(response));
                    var appContainer = $('.recipes-wrapper #recipes', tempDom);
                    count = appContainer.find(".recipe-tumbnail").length;
                    if (count === 0) {
                        window.history.pushState("object or string", "Title", window.location.search.replace('page='+page,'page='+(--page)));
                        scrollBlocked = true;
                    }
                    else {
                      $( ".recipes-wrapper" ).append( appContainer);
                      addPopover();
                    }
                    loading = false;

                }
            });
        }
    });

            var ingredients = new Bloodhound({
              datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              remote: {
                url: "/getIngredients/?searchString=%QUERY",
                wildcard: '%QUERY',
                ajax : {
                    type: "GET"
                }
            }
            });
            ingredients.initialize();

            var elt = $('#id_ingredients');
            var ingredientsString = elt.val();
            elt.tagsinput({
              itemValue: 'value',
              itemText: 'name',
              typeaheadjs: {
                limit: 20,
                name: 'ingredients',
                displayKey: 'name',
                source: ingredients.ttAdapter()
              }
            });

            var elt1 = $('#id_no_ingredients');
            var no_ingredientsString = elt1.val();
            elt1.tagsinput({
              itemValue: 'value',
              itemText: 'name',
              typeaheadjs: {
                limit: 20,
                name: 'ingredients',
                displayKey: 'name',
                source: ingredients.ttAdapter()
              }
            });
            if (ingredientsString) {
              var ingredientsArray = JSON.parse(ingredientsString);
              if (ingredientsArray) {
                ingredientsArray.forEach(function(item, i, ingredientsArray) {
                  elt.tagsinput('add', item);
                });
              }

            }
            if (no_ingredientsString) {
              var no_ingredientsArray = JSON.parse(no_ingredientsString);
              if (no_ingredientsArray) {
                no_ingredientsArray.forEach(function(item, i, no_ingredientsArray) {
                  elt1.tagsinput('add', item);
                });
              }
            }
});
