!function ($) {
    $.fn.imageselect = function(options) {
        var $this = $(this);

        $(this).hide();

        var currentValues = $this.val() || [];
        var label = options.label || "default label";

        $("<div class='btn-group'></div>").insertAfter($(this));

        var container = $(this).next(".btn-group");

        container.append("<label>" + label + "<span class='glyphicon glyphicon-menu-right' aria-hidden='true'></span></label>");
        container.append("<ul class='imageselect-list'></ul>");

        var labelDiv = container.find('label');
        var labelSpan = labelDiv.find('span');

        var ul = container.find(".imageselect-list");

        $(this).children().each(function(index, element) {
            var $element = $(element);
            // Support optgroups and options without a group simultaneously.
            var tag = $element.prop('tagName')
                .toLowerCase();

            if (tag === 'option') {
              var name = $element.prop('text');
              var value = $element.prop('value');
              var index = currentValues.indexOf(value);
              var li = $('<li/>')
                  .addClass('imageselect-item')
                  .attr('role', 'menuitem')
                  .attr('value', value)
                  .appendTo(ul);
              if (index > -1) {
                li.addClass("selected");
              }
              var image = $('<img/>')
                  .attr('src', '/static/img/imageselect/' + $this.attr('id') + value + '.png')
                  .text(name)
                  .appendTo(li);
               var label = $('<div/>')
                   .addClass('imageselect-label')
                   .text(name)
                   .appendTo(li);
            }
            // Other illegal tags will be ignored.

        });

        labelDiv.click(function() {
          labelSpan.toggleClass('glyphicon-menu-right');
          labelSpan.toggleClass('glyphicon-menu-down');
          ul.slideToggle();
        });

        ul.find("li").click(function () {

          $(this).toggleClass("selected");
          var value = $(this).attr('value');
          var index = currentValues.indexOf(value);
          if ($(this).hasClass("selected")) {
            if (index === -1) {
              currentValues.push(value);
            }
          } else {
            if (index > -1) {
                currentValues.splice(index, 1);
            }
          }
          $this.val(currentValues);
        });

        return this;
    };
}(window.jQuery);
