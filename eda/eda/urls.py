from django.urls import path, re_path
from django.conf.urls import handler400, handler500
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import RedirectView, ListView

import recipes.views

admin.autodiscover()

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^$', RedirectView.as_view(url='/guessrecipe/')),
    re_path(r'^drf_recipes/$', recipes.views.RecipeListApiView.as_view(), name='recipe-list'), #API
    re_path(r'^getIngredients/', recipes.views.getIngredients),
    re_path(r'^getIngredientsList/', recipes.views.getIngredientsList),
    re_path(r'^sendMail/', recipes.views.sendMail),
    re_path(r'^guessrecipe/', recipes.views.GuessRecipeView.as_view(),
        name='guessrecipe_view',),
    re_path(r'^guesselastic/', recipes.views.GuessElasticView.as_view(), 
        name='guesselastic_view'),
]
handler404 = recipes.views.error_404
handler500 = recipes.views.error_50x

urlpatterns += staticfiles_urlpatterns()
