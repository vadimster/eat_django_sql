from .settings import *

# Test runner with no database creation
TEST_RUNNER = 'recipes.testrunner.NoDbTestRunner'