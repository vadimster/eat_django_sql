import random
import operator
import time
from itertools import combinations
from functools import reduce

from django.test import Client, TestCase

from jinja2 import Template as Jinja2Template
from django.test import signals

#jinja2 render for response.context things
#note - this code can be run only once
ORIGINAL_JINJA2_RENDERER = Jinja2Template.render
def instrumented_render(template_object, *args, **kwargs):
    context = dict(*args, **kwargs)
    signals.template_rendered.send(
                            sender=template_object,
                            template=template_object,
                            context=context
                        )
    return ORIGINAL_JINJA2_RENDERER(template_object, *args, **kwargs)
Jinja2Template.render = instrumented_render

class TestGuessRecipeView(TestCase):
    #def test_random_url(self):
    #    c = Client()
    #    url = make_random_url()
    #    resp = c.get(url)
    #    print ('testing guessrecipe_view with random url: {}'.format(url))
    #    self.assertEqual(resp.status_code, 200)

    #def test_search_url(self):
    #    c = Client()
    #    search = 'пирог'
    #    url = make_url(search=search)
    #    resp = c.get(url)
    #    recipes = resp.context['recipes']
    #    self.assertEqual(check_recipes(recipes=recipes, search=search), True)

    def test_multiple_variations_postgres(self): ##guessrecipe test
        c = Client()
        variables = {'search': 'пирог', 'ingrs_ids': [21,22], 'only_all_ingrs': 1, 'no_ingrs_ids': [73, 37], 'r_types': [0,1], \
            'ingr_type': [0, 3], 'timelencat': [2,4], 'cuisine': [0, 1]}
        comblist = [[list(i) for i in combinations(variables.keys(), k)] for k in range(1,9)]
        comblist = reduce(operator.add, comblist) #combinations of multiple arguments
        print ('testing guessrecipe_view with {} numbers of urls'.format(len(comblist)))
        for combo in comblist:
            search = variables['search'] if 'search' in combo else None
            ingrs_ids = variables['ingrs_ids'] if 'ingrs_ids' in combo else None
            only_all_ingrs = variables['only_all_ingrs'] if 'only_all_ingrs' in combo else None
            no_ingrs_ids = variables['no_ingrs_ids'] if 'no_ingrs_ids' in combo else None
            r_types = variables['r_types'] if 'r_types' in combo else None
            ingr_type = variables['ingr_type'] if 'ingr_type' in combo else None
            cuisine = variables['cuisine'] if 'cuisine' in combo else None
            timelencat = variables['timelencat'] if 'timelencat' in combo else None
            url = make_url(search=search, ingrs_ids=ingrs_ids, only_all_ingrs=only_all_ingrs, \
                no_ingrs_ids=no_ingrs_ids, r_types=r_types, ingr_type=ingr_type, timelencat=timelencat, cuisine=cuisine)
            print ('testing guessrecipe_view with {} of {} url: {}'.format(comblist.index(combo) + 1, len(comblist), url))
            start_time = time.time()
            resp = c.get(url)
            recipes = resp.context['recipes']
            res = check_recipes(recipes=recipes, search=search, ingrs_ids=ingrs_ids, \
                only_all_ingrs=only_all_ingrs, no_ingrs_ids=no_ingrs_ids, r_types=r_types, \
                ingr_type=ingr_type, cuisine=cuisine)
            print ('total time: {}'.format(time.time()-start_time))
            self.assertEqual(res, True)
        self.assertEqual(True, True)
    def test_multiple_variations_elastic(self): ##guesselastic test
        c = Client()
        variables = {'search': 'пирог', 'ingrs_ids': [21,22], 'only_all_ingrs': 1, 'no_ingrs_ids': [73, 37], 'r_types': [0,1], \
            'ingr_type': [0, 3], 'timelencat': [2,4], 'cuisine': [0, 1]}
        comblist = [[list(i) for i in combinations(variables.keys(), k)] for k in range(1,9)]
        comblist = reduce(operator.add, comblist) #combinations of multiple arguments
        print ('testing guesselastic_view with {} numbers of urls'.format(len(comblist)))
        for combo in comblist:
            search = variables['search'] if 'search' in combo else None
            ingrs_ids = variables['ingrs_ids'] if 'ingrs_ids' in combo else None
            only_all_ingrs = variables['only_all_ingrs'] if 'only_all_ingrs' in combo else None
            no_ingrs_ids = variables['no_ingrs_ids'] if 'no_ingrs_ids' in combo else None
            r_types = variables['r_types'] if 'r_types' in combo else None
            ingr_type = variables['ingr_type'] if 'ingr_type' in combo else None
            cuisine = variables['cuisine'] if 'cuisine' in combo else None
            timelencat = variables['timelencat'] if 'timelencat' in combo else None
            url = make_url(search=search, ingrs_ids=ingrs_ids, only_all_ingrs=only_all_ingrs, \
                no_ingrs_ids=no_ingrs_ids, r_types=r_types, ingr_type=ingr_type, timelencat=timelencat, cuisine=cuisine)
            url = url.replace('guessrecipe', 'guesselastic')
            print ('testing guesselastic_view with {} of {} url: {}'.format(comblist.index(combo) + 1, len(comblist), url))
            start_time = time.time()
            resp = c.get(url)
            recipes = resp.context['recipes']
            res = check_recipes(recipes=recipes, search=search, ingrs_ids=ingrs_ids, \
                only_all_ingrs=only_all_ingrs, no_ingrs_ids=no_ingrs_ids, r_types=r_types, \
                ingr_type=ingr_type, cuisine=cuisine)
            print ('total time: {}'.format(time.time()-start_time))
            self.assertEqual(res, True)
        self.assertEqual(True, True)


def check_recipes(recipes, search=None, ingrs_ids=None, only_all_ingrs=None, no_ingrs_ids=None, \
        r_types=None, ingr_type=None, timelencat=None, cuisine=None):
    for r in recipes:
        if search:
            if not search in r.name.lower(): #.lower() for elasticsearch, doesnt really matter
                return False
        if ingrs_ids:
            if only_all_ingrs:
                if not set(ingrs_ids).issubset(set([i.short_id for i in r.ingrs.all()])):
                    return False
            else:
                if not set(ingrs_ids)&set([i.short_id for i in r.ingrs.all()]):
                    return False
        if no_ingrs_ids:
            if set(no_ingrs_ids)&set([i.short_id for i in r.ingrs.all()]):
                return False
        if r_types:
            if not r.recipe_type.short_id in r_types:
                return False
        if ingr_type:
            if not set(ingr_type)&set([i.category for i in r.ingrs.all()]):
                return False
        if timelencat:
            if 0 in timelencat:
                if r.total_mins > 5:
                    return False
            if 1 in timelencat:
                if r.total_mins > 10:
                    return False
            if 2 in timelencat:
                if r.total_mins > 30:
                    return False
            if 3 in timelencat:
                if r.ingrs_len > 5:
                    return False
            if 4 in timelencat:
                if r.ingrs_len > 10:
                    return False
        if cuisine:
            if not r.cuisine.short_id in cuisine:
                return False
    return True

def make_url(search, ingrs_ids=None, only_all_ingrs=None, no_ingrs_ids=None, r_types=None, ingr_type=None, \
        timelencat=None, cuisine=None):
    url = '/guessrecipe/?search=&ingredients=&no_ingredients=&page=0'
    if search:
        pos = url.index('search=')
        url = url[:pos + len('search=')] + search + url[pos + len('search='):]
    if ingrs_ids:
        value = ','.join([str(i) for i in ingrs_ids])
        pos = url.index('ingredients=')
        url = url[:pos + len('ingredients=')] + value + url[pos + len('ingredients='):]
        if only_all_ingrs:
            url += '&only_all_ingrs=on'
    if no_ingrs_ids:
        value = ','.join([str(i) for i in no_ingrs_ids])
        pos = url.index('no_ingredients=')
        url = url[:pos + len('no_ingredients=')] + value + url[pos + len('no_ingredients='):]
    if r_types:
        for value in r_types:
            url += '&r_types=' + str(value)
    if ingr_type:
        for value in ingr_type:
            url += '&ingredient_type=' + str(value)
    if timelencat:
        for value in timelencat:
            url += '&timelelncategory=' + str(value)
    if cuisine:
        for value in cuisine:
            url += '&cuisine=' + str(value)
    return url

def make_random_url():
    url = '/guessrecipe/?search=&ingredients=&no_ingredients=&page=0'
    prop_list = ['search', 'ingredients', 'no_ingredients', 'r_types', 'ingredient_type', 'timelencategory', 'cuisine']
    test_props = random.sample(prop_list, random.randint(0, len(prop_list)))
    for prop in test_props:
        if prop == 'search':
            value = random.choice(['пирог', 'суп', 'жаркое', 'паста'])
            pos = url.index('search=')
            url = url[:pos + len('search=')] + value + url[pos + len('search='):]
        if prop == 'ingredients':
            value = ','.join([str(i) for i in random.sample([21, 22, 82, 28, 7], random.randint(0, len([21, 22, 82, 28, 7])))])
            pos = url.index('ingredients=')
            url = url[:pos + len('ingredients=')] + value + url[pos + len('ingredients='):]
            if_only_all_ingredients = random.choice([0, 1])
            if if_only_all_ingredients:
                url += '&only_all_ingrs=on'
        if prop == 'no_ingredients=':
            value = ','.join([str(i) for i in random.sample([73, 337, 12, 48, 705], random.randint(0, len([73, 337, 12, 48, 705])))])
            pos = url.index('no_ingredients=')
            url = url[:pos + len('no_ingredients=')] + value + url[pos + len('no_ingredients='):]
        if prop == 'r_types':
            values = random.sample(list(range(0,8)), random.randint(1, 8))
            for value in values:
                url += '&r_types=' + str(value)
        if prop == 'ingredient_type':
            values = random.sample(list(range(0,10)), random.randint(1, 10))
            for value in values:
                url += '&ingredient_type=' + str(value)
        if prop == 'timelencategory':
            values = random.sample(list(range(0,5)), random.randint(1, 5))
            for value in values:
                url += '&timelencategory=' + str(value)
        if prop == 'cuisine':
            values = random.sample(list(range(0,106)), random.randint(1, 3))
            for value in values:
                url += '&cuisine=' + str(value)
    return url