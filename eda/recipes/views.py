import time
import json
import logging
import datetime
import requests
import os
import os.path

from django.shortcuts import render, render_to_response
from django.views.generic import FormView
from django.template.defaulttags import register
from django.http import HttpResponse, Http404, JsonResponse
from django.core.mail import send_mail
from rest_framework import generics

from recipes.models import Ingredient, Recipe, RecipeType, Cuisine, IngredientCategory, Suggestion
from recipes.forms import IngrListForm
from recipes.serializers import RecipeSerializer

from .eda_funcs import dec_r_type, dec_ingr_type, dec_cusi, dec_timelen, time_path, form_meta, yaspeller, \
    form_suggestions, form_query_elastic, form_query_postgres, get_cleaned_data, handle_api_params

logger = logging.getLogger(__name__)

@register.filter(name='get')
def get(d, k):
    return d.get(k, None)

@register.filter(name='t_path')
def t_path(pub_date):
    return time_path(pub_date)

def sendMail(request):
    name = request.POST.get('name', '')
    msg = request.POST.get('msg', '')
    if msg and name:
       send_mail(name,msg,'support@kurasgrechey.ru',['support@kurasgrechey.ru'],fail_silently=False)
       return HttpResponse("ok")
    return HttpResponse("error")

def getIngredients(request):
    searchString =  request.GET.get('searchString','')
    if len(searchString) < 4:
        ingr_list = [{"value":i.short_id, "name":i.name} for i in Ingredient.objects.filter(name__startswith=searchString.lower())]
    else:
        ingr_list = [{"value":i.short_id, "name":i.name} for i in Ingredient.objects.filter(name__contains=searchString.lower())]
    json_data = json.dumps(ingr_list)
    return HttpResponse(json_data, content_type="application/json")

def getIngredientsList(request):
    searchList =  request.GET.get('searchList','') #must be a list
    ingr_list = [{"value":i.short_id, "name":i.name} for i in [Ingredient.objects.filter(short_id=int(j)) for j in searchList]]
    json_data = json.dumps(ingr_list)
    return HttpResponse(json_data, content_type="application/json")

class GuessElasticView(FormView):
    template_name = 'recipes/guess_recipe.html'
    mobile_template_name = 'recipes/guess_recipe_mobile.html' #TODO
    form_class = IngrListForm

    def get(self, request, *args, **kwargs):
        form = self.form_class(request.GET)
        if request.user_agent.is_mobile:
            self.template_name = self.mobile_template_name #TODO
        if form.is_valid():
            start_time = time.time()
            current_url = request.build_absolute_uri()
            display_results = 48

            ingredients_id, no_ingredients_id, only_all_ingrs, r_types, cuisine, \
                ingredient_type, timelencategory, search = get_cleaned_data(form)

            search_spell, url_spell = yaspeller(search, current_url)

            page =  request.GET.get('page','')
            page = int(page) if len(page) else 0

            ingredients_dict = [{'value':i.short_id, 'name':i.name} for i in Ingredient.objects.filter(short_id__in=ingredients_id)] if len(ingredients_id) else []
            no_ingredients_dict = [{'value':i.short_id, 'name':i.name} for i in Ingredient.objects.filter(short_id__in=no_ingredients_id)] if len(no_ingredients_id) else []

            meta_title, meta_descr = form_meta(r_types, cuisine, ingredient_type, timelencategory)

            recipes = form_query_elastic(ingredients_id, only_all_ingrs, r_types, cuisine, search, no_ingredients_id, \
                ingredient_type, timelencategory, page, display_results)
            logger.debug('recipe-len: {}\n'.format(len(recipes)))

            suggestion_url, suggestion_text = form_suggestions()

            c = {'form': form, 'storage': 'elastic', 'recipes': recipes, 'only_all_ingrs': only_all_ingrs, 
                'ingredients_dict': json.dumps(ingredients_dict),'no_ingredients_dict': json.dumps(no_ingredients_dict),
                'suggestion_url' : suggestion_url,'suggestion_text' : suggestion_text, 'current_url': current_url,
                'meta_title': meta_title, 'meta_descr': meta_descr, 'search_spell': search_spell, 'url_spell': url_spell, 'exec_time':time.time()-start_time}
            return render(request, self.template_name, c)
        return render(request, self.template_name, {'form': form})


class GuessRecipeView(FormView):
    template_name = 'recipes/guess_recipe.html'
    mobile_template_name = 'recipes/guess_recipe_mobile.html'
    form_class = IngrListForm

    def get(self, request, *args, **kwargs):
        form = self.form_class(request.GET)
        if request.user_agent.is_mobile:
            self.template_name = self.mobile_template_name #TODO
        if form.is_valid():
            start_time = time.time()
            current_url = request.build_absolute_uri()
            display_results = 48

            ingredients_id, no_ingredients_id, only_all_ingrs, r_types, cuisine, \
                ingredient_type, timelencategory, search = get_cleaned_data(form)

            search_spell, url_spell = yaspeller(search, current_url)

            page =  request.GET.get('page','')
            page = int(page) if len(page) else 0

            ingredients_dict = [{'value':i.short_id, 'name':i.name} for i in Ingredient.objects.filter(short_id__in=ingredients_id)] if len(ingredients_id) else []
            no_ingredients_dict = [{'value':i.short_id, 'name':i.name} for i in Ingredient.objects.filter(short_id__in=no_ingredients_id)] if len(no_ingredients_id) else []

            meta_title, meta_descr = form_meta(r_types, cuisine, ingredient_type, timelencategory)

            recipes = form_query_postgres(ingredients_id, only_all_ingrs, r_types, cuisine, search, \
                no_ingredients_id, ingredient_type, timelencategory, page, display_results)
            logger.debug('recipe-len: {}\n'.format(len(recipes)))

            suggestion_url, suggestion_text = form_suggestions()

            c = {'form': form, 'storage': 'postgres', 'recipes': recipes, 'only_all_ingrs': only_all_ingrs, 
                'ingredients_dict': json.dumps(ingredients_dict),'no_ingredients_dict': json.dumps(no_ingredients_dict),
                'suggestion_url' : suggestion_url,'suggestion_text' : suggestion_text, 'current_url': current_url,
                'meta_title': meta_title, 'meta_descr': meta_descr, 'search_spell': search_spell, 'url_spell': url_spell, 'exec_time':time.time()-start_time}
            return render(request, self.template_name, c)
        return render(request, self.template_name, {'form': form})

class RecipeListApiView(generics.ListAPIView):
    serializer_class = RecipeSerializer

    def get_queryset(self):
        params = self.request.query_params
        search, ingrs_ids, only_all_ingrs, no_ingrs_ids, r_types, ingr_type, \
            cuisine, timelencat, iselastic = handle_api_params(params)
        if iselastic:
            recipes = form_query_elastic(ingrs_ids, only_all_ingrs, r_types, cuisine, search, \
                no_ingrs_ids, ingr_type, timelencat, 0, 48)
            return recipes
        else:
            recipes = form_query_postgres(ingrs_ids, only_all_ingrs, r_types, cuisine, search, \
                no_ingrs_ids, ingr_type, timelencat, 0, 48)
            return recipes

def get_obj_or_404(klass, *args, **kwargs):
    try:
        return klass.objects.get(*args, **kwargs)
    except klass.DoesNotExist:
        raise Http404

def error_404(request):
    return render(request, '404.html', {})

def error_50x(request):
    return render(request, '50x.html', {})