import requests
import operator
from datetime import datetime, timedelta
from random import randint
from functools import reduce

from django.db.models import Count
from haystack.query import SearchQuerySet as SQS

from recipes.models import Ingredient, Recipe, RecipeType, Cuisine, IngredientCategory, Suggestion

dec_r_type = {u"супы и бульоны": u"супов и бульонов",
              u"основные блюда": u"основных блюд",
              u"салаты": u"салатов",
              u"закуски": u"закусок",
              u"напитки": u"напитков",
              u"соусы и маринады": u"соусов и маринадов",
              u"выпечка и десерты": u"выпечки и десертов",
              u"другое": u"другого",}

dec_ingr_type = {u"баранина": u"баранины",
                 u"говядина": u"говядины",
                 u"курица": u"курицы",
                 u"овощи": u"овощей",
                 u"паста": u"пасты",
                 u"рис, крупы": u"риса, круп",
                 u"рыба и морепродукты": u"рыбы и морепродуктов",
                 u"свинина": u"свинины",
                 u"фрукты": u"фруктов",}

dec_timelen = {0: u'5',
              1: u'10',
              2: u'30',
              3: u'5',
              4: u'10',
              }

dec_cusi = {
u'чешская' :u'чешской',
u'индонезийская' :u'индонезийской',
u'европейская' :u'европейской',
u'словенская' :u'словенской',
u'башкирская' :u'башкирской',
u'бельгийская' :u'бельгийской',
u'карибская' :u'карибской',
u'коми' :u'коми',
u'иорданская' :u'иорданской',
u'авторская' :u'авторской',
u'итальянская' :u'итальянской',
u'индийская' :u'индийской',
u'китайская' :u'китайской',
u'мексиканская' :u'мексиканской',
u'финская' :u'финской',
u'средиземноморская' :u'средиземноморской',
u'швейцарская' :u'швейцарской',
u'канадская' :u'канадской',
u'латиноамериканская' :u'латиноамериканской',
u'сербская' :u'сербской',
u'киргизская' :u'киргизской',
u'шотландская' :u'шотландской',
u'карельская' :u'карельской',
u'арабская' :u'арабской',
u'монгольская' :u'монгольской',
u'корейская' :u'корейской',
u'словацкая' :u'словацкой',
u'перуанская' :u'перуанской',
u'казахская' :u'казахской',
u'боснийская' :u'боснийской',
u'болгарская' :u'болгарской',
u'малазийская' :u'малазийской',
u'грузинская' :u'грузинской',
u'украинская' :u'украинской',
u'аргентинская' :u'аргентинской',
u'молдавская' :u'молдавской',
u'скандинавская' :u'скандинавской',
u'ирландская' :u'ирландской',
u'гавайская' :u'гавайской',
u'немецкая' :u'немецкой',
u'иракская' :u'иракской',
u'южноафриканская' :u'южноафриканской',
u'кипрская' :u'кипрской',
u'бразильская' :u'бразильской',
u'курдская' :u'курдской',
u'норвежская' :u'норвежской',
u'калмыцкая' :u'калмыцкой',
u'греческая' :u'греческой',
u'таджикская' :u'таджикской',
u'французская' :u'французской',
u'хорватская' :u'хорватской',
u'абхазская' :u'абхазской',
u'тунисская' :u'тунисской',
u'иранская' :u'иранской',
u'британская' :u'британской',
u'азербайджанская' :u'азербайджанской',
u'турецкая' :u'турецкой',
u'румынская' :u'румынской',
u'испанская' :u'испанской',
u'американская' :u'американской',
u'эстонская' :u'эстонской',
u'уральская' :u'уральской',
u'шведская' :u'шведской',
u'польская' :u'польской',
u'австрийская' :u'австрийской',
u'армянская' :u'армянской',
u'латвийская' :u'латвийской',
u'литовская' :u'литовской',
u'сирийская' :u'сирийской',
u'азиатская' :u'азиатской',
u'датская' :u'датской',
u'африканская' :u'африканской',
u'дагестанская' :u'дагестанской',
u'латышская' :u'латышской',
u'бурятская' :u'бурятской',
u'венгерская' :u'венгерской',
u'узбекская' :u'узбекской',
u'баскская' :u'баскской',
u'прибалтийская' :u'прибалтийской',
u'кубинская' :u'кубинской',
u'египетская' :u'египетской',
u'югославская' :u'югославской',
u'осетинская' :u'осетинской',
u'тайская' :u'тайской',
u'паназиатская' :u'паназиатской',
u'австралийская' :u'австралийской',
u'ближневосточная' :u'ближневосточной',
u'татарская' :u'татарской',
u'вьетнамская' :u'вьетнамской',
u'чилийская' :u'чилийской',
u'голландская' :u'голландской',
u'еврейская' :u'еврейской',
u'кавказская' :u'кавказской',
u'английская' :u'английской',
u'белорусская' :u'белорусской',
u'русская' :u'русской',
u'магриба' :u'магриба',
u'интернациональная' :u'интернациональной',
u'японская' :u'японской',
u'марокканская' :u'марокканской',
u'португальская' :u'португальской',
u'черногорская' :u'черногорской',
u'исландская' :u'исландской',
u'валлийская' :u'валлийской',
u'ливанская' :u'ливанской',
u'израильская' :u'израильской',
}

def translit(locallangstring):
    conversion = {
        u'\u0410' : 'A',    u'\u0430' : 'a',
        u'\u0411' : 'B',    u'\u0431' : 'b',
        u'\u0412' : 'V',    u'\u0432' : 'v',
        u'\u0413' : 'G',    u'\u0433' : 'g',
        u'\u0414' : 'D',    u'\u0434' : 'd',
        u'\u0415' : 'E',    u'\u0435' : 'e',
        u'\u0401' : 'Yo',   u'\u0451' : 'yo',
        u'\u0416' : 'Zh',   u'\u0436' : 'zh',
        u'\u0417' : 'Z',    u'\u0437' : 'z',
        u'\u0418' : 'I',    u'\u0438' : 'i',
        u'\u0419' : 'Y',    u'\u0439' : 'y',
        u'\u041a' : 'K',    u'\u043a' : 'k',
        u'\u041b' : 'L',    u'\u043b' : 'l',
        u'\u041c' : 'M',    u'\u043c' : 'm',
        u'\u041d' : 'N',    u'\u043d' : 'n',
        u'\u041e' : 'O',    u'\u043e' : 'o',
        u'\u041f' : 'P',    u'\u043f' : 'p',
        u'\u0420' : 'R',    u'\u0440' : 'r',
        u'\u0421' : 'S',    u'\u0441' : 's',
        u'\u0422' : 'T',    u'\u0442' : 't',
        u'\u0423' : 'U',    u'\u0443' : 'u',
        u'\u0424' : 'F',    u'\u0444' : 'f',
        u'\u0425' : 'H',    u'\u0445' : 'h',
        u'\u0426' : 'Ts',   u'\u0446' : 'ts',
        u'\u0427' : 'Ch',   u'\u0447' : 'ch',
        u'\u0428' : 'Sh',   u'\u0448' : 'sh',
        u'\u0429' : 'Sch',  u'\u0449' : 'sch',
        u'\u042a' : '"',    u'\u044a' : '"',
        u'\u042b' : 'Y',    u'\u044b' : 'y',
        u'\u042c' : '\'',   u'\u044c' : '\'',
        u'\u042d' : 'E',    u'\u044d' : 'e',
        u'\u042e' : 'Yu',   u'\u044e' : 'yu',
        u'\u042f' : 'Ya',   u'\u044f' : 'ya',
    }
    translitstring = []
    for c in locallangstring:
        translitstring.append(conversion.setdefault(c, c))
    return ''.join(translitstring)


def time_path(pub_date):
    now = datetime.now().date()
    delta = now - pub_date.date()
    if delta.days == 0:
        return u'Сегодня'
    if delta.days == 1:
        return u'Вчера'
    if delta.days == 2:
        return u'Позавчера'
    if delta.days == 21:
        return u'%d день назад' % delta.days
    if delta.days in [3,4,22,23,24]:
        return u'%d дня назад' % delta.days
    if delta.days < 30:
        return u'%d дней назад' % delta.days
    if delta.days < 30*2:
        return u'%d месяц назад' % int(delta.days / 30)
    if delta.days < 30*5:
        return u'%d месяца назад' % int(delta.days / 30)
    if delta.days < 30*12:
        return u'%d месяцев назад' % int(delta.days / 30)
    if delta.days < 30*12*2:
        return u'%d год назад' % int(delta.days / (30 * 12))
    if delta.days < 30*12*5:
        return u'%d года назад' % int(delta.days / (30 * 12))
    if delta.days < 30*12*500:
        return u'%d лет назад' % int(delta.days / (30 * 12))



def form_meta(r_type, cuisine, ingr_type, timelencategory):
            meta_title = ''
            meta_descr = ''
            if len(r_type):
                r_type_str = [dec_r_type[i.name] for i in RecipeType.objects.filter(short_id__in=r_type)]
                meta_title += ' ' + ', '.join(r_type_str)
            if len(cuisine):
                cuisine_str = [dec_cusi[i.name] for i in Cuisine.objects.filter(short_id__in=cuisine)]
                meta_title += ' ' + ', '.join(cuisine_str) + ' кухни'
            if len(ingr_type):
                ingr_type_str = [dec_ingr_type[i.name] for i in IngredientCategory.objects.filter(short_id__in=ingr_type)]
                meta_title += ' из ' + ', '.join(ingr_type_str)
            if len(timelencategory):
                timelencategory_str = ''
                if set(timelencategory) & set([0,1,2]):
                    timelencategory_str += ' за ' + \
                        ','.join([dec_timelen[i] for i in list(set(timelencategory) & set([0,1,2]))]) + ' минут'
                if set(timelencategory) & set([3,4]):
                    timelencategory_str += ' из ' + \
                        ','.join([dec_timelen[i] for i in list(set(timelencategory) & set([0,1,2]))]) + ' ингредиентов'
                meta_title += timelencategory_str
            if meta_title: 
                meta_title = 'Рецепты' + meta_title 
                meta_descr = 'Поиск рецептов ' + meta_title[8:]
            else: 
                meta_title = 'Кура с гречей - поиск рецептов по ингредиентам'
                meta_descr = 'Поиск рецептов по типу и количеству ингредиентов, типу блюда, национальной кухне и времени приготовления'
            return meta_title, meta_descr

def yaspeller(spell, current_url):
    r_speller = requests.get('http://speller.yandex.net/services/spellservice.json/checkText', params={'text':spell,'lang':'ru'})
    search_spell = spell
    url_spell = ''
    if r_speller.status_code == 200 and len(r_speller.json()):
        for word in r_speller.json():
            if len(word['s']):
                search_spell = search_spell.replace(word['word'], word['s'][0])
    if search_spell == spell:
        search_spell = ''
    else:
        url_spell = current_url[:current_url.index('search=')+len('search=')] + search_spell + current_url[current_url.index('&ingredients'):]
    return search_spell, url_spell

def form_suggestions():
    if Suggestion.objects.count() > 0:
        a = randint(0,Suggestion.objects.count() - 1)
        suggestion = Suggestion.objects.all()[a]
        if suggestion:
            suggestion_url = suggestion.url
            suggestion_text = suggestion.html_text
        else:
            suggestion_url = ''
            suggestion_text = ''
    return suggestion_url, suggestion_text

def form_query_elastic(ingrs_ids, only_all_ingrs, r_types, cuisine, search, no_ingrs_ids, ingr_type, \
        timelencat, page, display_results):
    recipes = []
    and_list = [] #queryset list
    if len(ingrs_ids):
        if only_all_ingrs:
            and_list.append(reduce(operator.and_, [SQS().filter(ingrs__in=[i]) for i in ingrs_ids])) #dirty
        else:
            and_list.append(SQS().filter(ingrs__in=ingrs_ids))
    if len(r_types):
        and_list.append(SQS().filter(recipe_type__in=r_types))
    if len(cuisine):
        and_list.append(SQS().filter(cuisine__in=cuisine))
    if len(search):
        #search = search.replace(',',' ') #what is this?
        #search = ' '.join(['\"'+z+'\"' for z in search.split() if len(z)])
        and_list.append(SQS().filter(name__contains=search))
    if len(no_ingrs_ids):
        and_list.append(SQS().exclude(ingrs__in=no_ingrs_ids))
    if len(ingr_type):
        and_list.append(SQS().filter(ingrs_category__in=ingr_type))
    if len(timelencat):
        if 0 in timelencat: and_list.append(SQS().filter(total_mins__lte=5))
        elif 1 in timelencat: and_list.append(SQS().filter(total_mins__lte=10))
        elif 2 in timelencat: and_list.append(SQS().filter(total_mins__lte=30))
        if 3 in timelencat: and_list.append(SQS().filter(ingrs_len__lte=5))
        elif 4 in timelencat: and_list.append(SQS().filter(ingrs_len__lte=10))
    if len(and_list) > 1:
        recipes = [r.object for r in reduce(operator.and_, and_list).order_by('-good_img')[page * display_results: (page + 1) * display_results]]
    elif len(and_list) == 1:
        recipes = [r.object for r in and_list[0].order_by('-good_img')[page * display_results: (page + 1) * display_results]]
    return recipes

def form_query_postgres(ingrs_ids, only_all_ingrs, r_types, cuisine, search, no_ingrs_ids, ingr_type, \
        timelencat, page, display_results):
    recipes = []
    and_list = [] #queryset list
    if len(ingrs_ids):
        if only_all_ingrs:
            #and_list.append(Recipe.objects.filter(ingrs__short_id__in=ingrs_ids).annotate(n=Count('ingrs')).filter(n=len(ingrs_ids)).distinct())
            and_list.append(reduce(operator.and_, [Recipe.objects.filter(ingrs__short_id__in=[i]) for i in ingrs_ids]).distinct())
        else:
            and_list.append(Recipe.objects.filter(ingrs__short_id__in=ingrs_ids).distinct())
    if len(r_types):
        and_list.append(Recipe.objects.filter(recipe_type__short_id__in=r_types).distinct())
    if len(cuisine):
        and_list.append(Recipe.objects.filter(cuisine__short_id__in=cuisine).distinct())
    if len(search):
        #search = search.replace(',',' ') #what is this?
        #search = ' '.join(['\"'+z+'\"' for z in search.split() if len(z)])
        and_list.append(Recipe.objects.filter(name__contains=search).distinct())
    if len(no_ingrs_ids):
        and_list.append(Recipe.objects.exclude(ingrs__short_id__in=no_ingrs_ids).distinct())
    if len(ingr_type):
         and_list.append(Recipe.objects.filter(ingrs__category__in=ingr_type).distinct())
    if len(timelencat):
        if 0 in timelencat: and_list.append(Recipe.objects.filter(total_mins__lte=5).distinct())
        elif 1 in timelencat: and_list.append(Recipe.objects.filter(total_mins__lte=10).distinct())
        elif 2 in timelencat: and_list.append(Recipe.objects.filter(total_mins__lte=30).distinct())
        if 3 in timelencat: and_list.append(Recipe.objects.filter(ingrs_len__lte=5).distinct())
        elif 4 in timelencat: and_list.append(Recipe.objects.filter(ingrs_len__lte=10).distinct())
        #if 3 in timelencategory: and_list.append(Recipe.objects.annotate(num_ingrs=Count('ingrs')).filter(num_ingrs__lte=5).distinct())
        #elif 4 in timelencategory: and_list.append(Recipe.objects.annotate(num_ingrs=Count('ingrs')).filter(num_ingrs__lte=10).distinct())
    if len(and_list) > 1:
        recipes = reduce(operator.and_, and_list).order_by('-good_img')[page * display_results: (page + 1) * display_results]
    elif len(and_list) == 1:
        recipes = and_list[0].order_by('-good_img')[page * display_results: (page + 1) * display_results]
    return recipes

def get_cleaned_data(form):
    temp = form.cleaned_data['ingredients']
    ingredients_id = [int(i) for i in temp.split(',')] if len(temp) else []
    temp = form.cleaned_data['no_ingredients']
    no_ingredients_id = [int(i) for i in temp.split(',')] if len(temp) else []
    only_all_ingrs = form.cleaned_data['only_all_ingrs']
    r_types = [int(i) for i in form.cleaned_data['r_types']]
    cuisine = [int(i) for i in form.cleaned_data['cuisine']]
    ingredient_type = [int(i) for i in form.cleaned_data['ingredient_type']]
    timelencategory = [int(i) for i in form.cleaned_data['timelencategory']]
    search = form.cleaned_data['search']
    return ingredients_id, no_ingredients_id, only_all_ingrs, r_types, cuisine, \
        ingredient_type, timelencategory, search

def handle_api_params(params):
    search = params['search'] if 'search' in params else ''
    ingrs_ids = [int(i) for i in params['ingrs_ids'].split(',')] if 'ingrs_ids' in params else []
    only_all_ingrs = int(params['only_all_ingrs']) if 'only_all_ingrs' in params else 0 #1 or 0
    no_ingrs_ids = [int(i) for i in params['no_ingrs_ids'].split(',')] if 'no_ingrs_ids' in params else []
    r_types = [int(i) for i in params['r_types'].split(',')] if 'r_types' in params else []
    ingr_type = [int(i) for i in params['ingr_type'].split(',')] if 'ingr_type' in params else []
    cuisine = [int(i) for i in params['cuisine'].split(',')] if 'cuisine' in params else []
    timelencat = [int(i) for i in params['timelencat'].split(',')] if 'timelencat' in params else []
    iselastic = int(params['elastic']) if 'elastic' in params else 0
    return search, ingrs_ids, only_all_ingrs, no_ingrs_ids, r_types, ingr_type, cuisine, timelencat, iselastic