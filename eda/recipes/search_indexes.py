import datetime
from django.utils import timezone
from haystack import indexes
from haystack.fields import CharField, IntegerField

from .models import Recipe

class RecipeIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(
        document=True, use_template=True,
        template_name='/search/recipe_text.txt')
    ingrs = indexes.MultiValueField()
    ingrs_len = indexes.IntegerField()
    ingrs_category = indexes.MultiValueField()
    name = indexes.CharField(model_attr='name')
    recipe_type = indexes.IntegerField()
    cuisine = indexes.IntegerField(null=True)
    total_mins = indexes.IntegerField(model_attr='total_mins', null=True)
    good_img = indexes.IntegerField(model_attr='good_img')

    def prepare_ingrs(self, obj):
        return [i.short_id for i in obj.ingrs.all()]

    def prepare_ingrs_len(self, obj):
        return len(obj.ingrs.all())

    def prepare_ingrs_category(self, obj):
        return list({i.category for i in obj.ingrs.all() if i.category is not None})

    def prepare_recipe_type(self, obj):
        return obj.recipe_type.short_id

    def prepare_cuisine(self,obj):
        return obj.cuisine.short_id if obj.cuisine else None

    def get_model(self):
        return Recipe

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()#filter(timestamp__lte=timezone.now())