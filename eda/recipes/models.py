from django.contrib.postgres.fields import ArrayField
from django.db import models

class Ingredient(models.Model):
    short_id = models.IntegerField()
    name = models.TextField()
    category = models.IntegerField(null=True)

    def __str__(self):
        return (self.name)

class IngredientCategory(models.Model):
    short_id = models.IntegerField()
    name = models.TextField()

    def __str__(self):
        return (self.name)

class RecipeType(models.Model):
    short_id = models.IntegerField()
    name = models.TextField()

    def __str__(self):
        return (self.name)

class Cuisine(models.Model):
    short_id = models.IntegerField()
    name = models.TextField()

    def __str__(self):
        return (self.name)

class TimeLenCategory(models.Model):
    short_id = models.IntegerField()
    name = models.TextField()

    def __str__(self):
        return (self.name)

class Suggestion(models.Model):
    url = models.TextField()
    html_text = models.TextField()

    def __str__(self):
        return (self.name)

class Recipe(models.Model):
    name = models.TextField()
    url = models.TextField()
    ingrs = models.ManyToManyField(Ingredient)
    ingrs_len = models.IntegerField()
    desc = models.TextField(null=True)
    site_id = models.IntegerField(null=True)
    recipe_type = models.ForeignKey(RecipeType, on_delete=models.SET_NULL, null=True) #cascade?
    cuisine = models.ForeignKey(Cuisine, on_delete=models.SET_NULL, null=True)
    total_mins = models.IntegerField(null=True)
    good_img = models.IntegerField()
    img_url = models.TextField()

    def __str__(self):
        return (self.name)