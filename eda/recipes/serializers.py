from rest_framework import serializers
from recipes.models import Recipe


class RecipeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Recipe
        fields = ('id', 'name', 'url', 'ingrs', 'ingrs_len', 'desc', 'site_id', 'recipe_type', 'cuisine', 'total_mins', 'good_img', 'img_url')