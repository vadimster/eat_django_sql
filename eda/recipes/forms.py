from operator import itemgetter
import ast
import datetime
import logging

from django import forms
from django.core.exceptions import ValidationError
from django.forms.models import inlineformset_factory

from recipes.models import Ingredient, RecipeType, Cuisine, IngredientCategory, TimeLenCategory
#from django.utils.safestring import mark_safe

#class HorizontalRadioRenderer(forms.RadioSelect.renderer):
#  def render(self):
#    return mark_safe(u'\n'.join([u'%s\n' % w for w in self]))

logger = logging.getLogger(__name__)

class IngrListForm(forms.Form):

    search = forms.CharField(label='', widget=forms.TextInput(attrs={'placeholder': u'Поиск по названию рецепта'}), required=False)

    ingredients = forms.CharField(label='',#label='Choice ingredients you already have:',
	   required=False,
       widget=forms.TextInput(attrs={'placeholder': u'Начните ввод'}),
       max_length=200
	)
    no_ingredients = forms.CharField(label='',#label='Choice ingredients you dont like:',
	   required=False,
       widget=forms.TextInput(attrs={'placeholder': u'Начните ввод'}),
       max_length=200
	)

    only_all_ingrs = forms.BooleanField(label=u'Только вместе', required=False)
    r_types = forms.MultipleChoiceField(choices=
        ((r_type[0], r_type[1].capitalize()) for r_type in RecipeType.objects.all().values_list('short_id', 'name')),
    label='', required=False)

    cuisine = forms.MultipleChoiceField(choices=
        ((cusi[0], cusi[1].capitalize()) for cusi in Cuisine.objects.order_by('name').values_list('short_id', 'name')),
    label='', required=False)

    ingredient_type = forms.MultipleChoiceField(
         choices=
         ((category[0], category[1].capitalize()) for category in IngredientCategory.objects.order_by('name').values_list('short_id', 'name')),
    label='', required=False)

    timelencategory = forms.MultipleChoiceField(
	choices= #TL_CHOICES,
	((timelen[0], timelen[1]) for timelen in TimeLenCategory.objects.values_list('short_id', 'name')),
	label='', required=False)
