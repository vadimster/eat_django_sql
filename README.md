# описание проекта

kurasgrechey - сервис комплексного поиска рецептов по различным параметрам (ингредиентам, типу кухни, времени приготовления и др.)

Идея приложения в том, что поиск осуществляется по мета-данным рецептов в базе, а рецепты в поисковой выдаче - это ссылки на кулинарные сайты, с которых и были взяты эти мета-данные, т.о. приложение представляет из себя поисковый агрегатор различных веб-сервисов с рецептами.

-Проектирование и заполнение бд: postgresql/haystack+postgresql+elasticsearch
Мета-данные рецептов(~150тыс) спаршены с нескольких крупных сайтов с помощью scrapy.

-Разработка поискового приложения: Django 2.0, python 3.4, Jinja2

-Построение архитектуры сверточной нейронной сети, классифицирующей изображения рецептов для формирования "красивой" поисковой выдачи: nolearn

-Разработка API получения списка рецептов по параметрам: Django REST Framework

-Настройка и установка приложения на сервер: docker, uwsgi, nginx

-git

Есть репозиторий с более старой версией проекта, реализованной с Django 1.8, python 2.7, mongoDB + mongoengine


![picture](eda/eda/static/img/preview1.png normal version)
![picture](eda/eda/static/img/preview21.png mobile version1)
![picture](eda/eda/static/img/preview22.png mobile version2)

# заметки

pip install -r requirements.txt

works with elasticsearch 2.4.6

tests for both normal psql search (/guessrecipe) and elastic (/guesselastic)

run with:

python manage.py test recipes.tests.TestGuessRecipeView --settings='eda.no_db_settings'

getrecipes API:

http 'http://127.0.0.1:8000/drf_recipes/?ingrs_ids=1,2,3,4&r_types=5,6&cuisine=7&search=пирог'

